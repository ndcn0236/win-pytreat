# Matlab vs. Python

This directory contains 5 demonstrations, each of which uses python and Matlab to solve the same problem:
1. `bloch` - a simple Bloch simulator that numerically integrates the Bloch equation for magnetisation dynamics
2. `fitting` - an example of using the Scipy package for fitting a biophysical model to noisy data
3. `migp` - an example of MIGP dimension reduction prior to group ICA on fMRI data
4. `partial_fourier` - a projection-onto-convex-sets Partial Fourier image reconstruction of k-space data that is partially sampled
5. `rbf` - simple example of fitting/interpolating noisy simulated data using radial basis functions

Each demonstration contain both `Matlab` and `python` code examples so you can compare the implementations.

## Required software

### FSL
The python code is contained in jupyter notebooks which can be run in the `fslpython` environment (requires `FSL`) using:

```
fslpython -m notebook
```

### Matlab

A `matlab` installation is required to run the Matlab examples.